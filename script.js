"use strict";

function setupMenu(menuElement) {
  menuElement.addEventListener("click", function setActive(event) {
    const target = event.target.closest(".tabs-title") || event.target;

    const activeTab = this.querySelector(".active");
    const activeTabData = target.dataset.tabName;

    const tabsContent = this.parentElement.querySelector(".tabs-content");
    const visibleContent = tabsContent.querySelector(".visible");
    const visibleContentData =
      tabsContent.querySelector(".visible").dataset.tabContent;

    if (!target.classList.contains("tabs-title")) return;

    if (activeTab !== target) {
      activeTab.classList.remove("active");
      target.classList.add("active");
    }

    if (visibleContentData !== activeTabData) {
      visibleContent.classList.remove("visible");
      tabsContent
        .querySelector(`[data-tab-content="${activeTabData}"]`)
        .classList.add("visible");
    }
  });
}

setupMenu(document.querySelector(".tabs"));
